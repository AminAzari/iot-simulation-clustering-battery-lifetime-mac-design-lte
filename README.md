This repository delivers simulator for the following paper:

Title: E2-MAC: Energy Efficient Medium Access for Massive M2M Communications

Authors: Guowang Miao, Senior Member, IEEE, Amin Azari, Student Member, IEEE, and Taewon Hwang, Senior Member, IEEE,

Download: http://kth.diva-portal.org/smash/get/diva2:955146/FULLTEXT01.pdf

Download simulator: https://www.researchgate.net/profile/Amin_Azari3/publication/321679810_simulator/data/5a2aa1e845851552ae7a6570/simulator.zip

Abstract: 
In this paper, we investigate energy-efficient clustering and medium access control for
cellular-based machine-to-machine (M2M) networks to minimize device energy consumption 
and prolong network battery lifetime. First, we present an accurate energy consumption 
model that considers both static and dynamic energy consumptions, and utilize this model
to derive the network lifetime. Second, we find the cluster size to maximize the network
lifetime and develop an energy-efficient cluster-head selection scheme. Furthermore, we
find feasible regions where clustering is beneficial in enhancing network lifetime. We
further investigate communications protocols for both intra- and inter-cluster communications.
While inter-cluster communications use conventional cellular access schemes, we develop an 
energy-efficient and load-adaptive multiple access scheme, called n-phase carrier sense multiple
access with collision avoidance (CSMA/CA), which provides a tunable tradeoff between energy
efficiency, delay, and spectral efficiency of the network. The simulation results show that
the proposed clustering, cluster-head selection, and communications protocol design outperform
the others in energy saving and significantly prolong the lifetimes of both individual nodes
and the whole M2M network.